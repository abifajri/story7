[![pipeline status](https://gitlab.com/abifajri/story7/badges/master/pipeline.svg)](https://gitlab.com/abifajri/story7/commits/master)  
[![coverage report](https://gitlab.com/abifajri/story7/badges/master/coverage.svg)](https://gitlab.com/abifajri/story7/commits/master)  

## Author
Abi Fajri Abdillah

## Heroku Link  
Story 7 | abifajri-story7.herokuapp.com  
Story 8 | abifajri-story7.herokuapp.com/story8/  
Story 9 | abifajri-story7.herokuapp.com/story9/  