const accordion = $(".accordion");
const lightSwitch = $("#light-switch");

$(document).ready(function(){
    accordion.each(function(){
        $(this).next().hide();
    });
});

lightSwitch.click(function(){
    if (lightSwitch.is(":checked")) {
        $("#style").prop("href", "static/css/style-dark.css");
    } else {
        $("#style").prop("href", "static/css/style.css");
    }
});

accordion.click(function(){
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this).next().slideUp(300);
    } else {
        accordion.each(function(){
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).next().slideUp(300);
            }
        });

        $(this).addClass("active");
        $(this).next().slideDown(300);
    }
});