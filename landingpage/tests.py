from django.test import TestCase
from django.test import TestCase, Client
from django.urls import reverse, resolve

import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from . import views

# Create your tests here.
class LandingPageTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_using_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landingpage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
    
class LandingPageFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):    
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # for local testing
        # self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

    # insert test here
    def test_js_functions(self):
        self.selenium.get('%s' % (self.live_server_url))

        time.sleep(1)
        check_conditions = self.selenium.find_element_by_css_selector('#light-switch')
        self.selenium.execute_script('arguments[0].click();', check_conditions)
        time.sleep(0.5)
        self.selenium.execute_script('arguments[0].click();', check_conditions)
        time.sleep(1)
        
        accordions = self.selenium.find_elements_by_class_name('accordion')
        for accordion in accordions:
            accordion.send_keys(Keys.RETURN)
            time.sleep(0.3)
            accordion.send_keys(Keys.RETURN)
        time.sleep(1)