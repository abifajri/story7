from django.test import TestCase
from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.http import HttpRequest


from . import views

# Create your tests here.
class Story9Test(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Story9Test, cls).setUpClass()
        cls.user = User.objects.create_user(
            'testuser', 'temp@gmail.com', 'pass1234'
        )
        cls.user.first_name = 'firstname'
        cls.user.last_name = 'lastname'
        cls.user.save()

    def test_story9_url_is_exist(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_using_user_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'user.html')

    def test_story9_using_user_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, views.user)

    def test_login_url_is_exists(self):
        response = self.client.get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_post_succeed(self):
        response = self.client.post('/story9/login/', {
                'username' : 'testuser',
                'password' : 'pass1234',
            })
        self.assertEqual(response.status_code, 302)
        response_home = self.client.get('/story9/')
        html = response_home.content.decode()
        self.assertIn(self.user.first_name, html)

    def test_login_post_fail(self):
        response = self.client.post('/story9/login/', {
                'username' : 'testnonuser',
                'password' : 'pass12345',
            })
        self.assertEqual(response.status_code, 200)
        html = response.content.decode()
        self.assertIn('Invalid', html)

    def test_logout(self):
        response = self.client.post('/story9/login/', {
                'username' : 'testuser',
                'password' : 'pass1234',
            })
        response = self.client.get('/story9/')
        html = response.content.decode()
        self.assertIn('Hello', html)
        response2 = self.client.get('/story9/logout/')
        self.assertEqual(response2.status_code, 302)
        response3 = self.client.get('/story9/')
        html2 = response3.content.decode()
        self.assertIn('Please Log In.', html2)
