from django.shortcuts import render, redirect
from datetime import datetime
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

# Create your views here.
def user(request):
    return render(request, 'user.html')

def login(request):
    if request.user is not None:
        if request.user.is_authenticated:
            return redirect('story9:user')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            request.session['login_timestamp'] = str(datetime.now())
            return redirect('story9:user')
        else:
            return render(request, 'login.html', {
                'error' : True
            })        

    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    auth_logout(request)
    return redirect('story9:user')