from django.test import TestCase
from django.test import TestCase, Client
from django.urls import reverse, resolve

from . import views

# Create your tests here.
class Story8Test(TestCase):
    def test_story8_url_is_exist(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_books_template(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'books.html')

    def test_story8_using_search_books_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.books)