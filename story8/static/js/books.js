window.addEventListener("DOMContentLoaded", function (event) {
    if (localStorage.length != 0) {
        let content = [];
        Object.keys(localStorage).forEach(function (key) {
            let item = JSON.parse(localStorage.getItem(key));
            content.push(`<tr>
                <td>${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="width: 10rem;" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 15rem" alt="...">`}</td>
                <td>${item.volumeInfo.title}</td>
                <td>${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</td>
                <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">More info</a></td>
            </tr>`)
        });
        document.querySelector(".body").innerHTML = '<tr>' + content.join(" ") + '</tr>';
    }
})

document.getElementById("searchBooks").addEventListener("submit", function (event) {
    event.preventDefault();
    document.querySelector(".body").innerHTML = '<br><p style="text-align: center">Loading...</p>'
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            localStorage.clear()
            if (JSON.parse(this.response).totalItems == 0) {
                document.querySelector(".body").innerHTML = '<br><p style="text-align: center">None</p>';
            } else {
                document.querySelector(".body").innerHTML = '<div class="card-columns">' + JSON.parse(this.response).items.map((item, index, itemsArray) => {
                    localStorage.setItem(item.id, JSON.stringify(item));
                    return `<tr>
                    <td>${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="width: 10rem;" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 15rem" alt="...">`}</td>
                    <td>${item.volumeInfo.title}</td>
                    <td>${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</td>
                    <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">More info</a></td>
                </tr>`;
                }).join(" ") + '</div>';
            }
        };
    };
    param = document.getElementById("id_Search_Box").value.trim().split(" ").join("_");
    xhttp.open("GET", `search_books/${param}/`, true);
    xhttp.setRequestHeader("X-CSRFToken", document.getElementsByName('csrfmiddlewaretoken')[0].value);
    xhttp.send();
})