from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
from .forms import Search_Box

# Create your views here.
def books(request):
    response = {
        'form': Search_Box
    }

    return render(request, 'books.html', response)

def search_books(request, param):
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + param).json()
    return JsonResponse(json)
